FROM erlang

RUN mkdir -p /build

WORKDIR /build

COPY src src

RUN erl -compile src/hello

CMD ["erl", "-noshell", "-s", "hello", "hello_world", "-s", "init", "stop"]

